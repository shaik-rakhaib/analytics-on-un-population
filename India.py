'''To plot India's population across the years'''
import csv
import matplotlib.pyplot as plt

india_year = {}


def calc_india_popu():
    '''calculates India's population across all the years'''
    with open('population.csv','r',encoding='utf-8') as file:
        csv_file=csv.DictReader(file)
        for row in csv_file:
            if row['Region']=='India':
                popul=float(row['Population'])
                india_year[row['Year']]=int(popul)

            
def plot_india_popu():
    '''plots India population across all the years'''
    plt.figure(figsize=(12, 6))
    plt.bar(list(india_year.keys()), list(india_year.values()))
    plt.title("Population of India across various Years")
    plt.xlabel("Years")
    plt.ylabel("Population")
    plt.xticks(rotation=90)
    plt.show()


def execute():
    calc_india_popu()
    plot_india_popu()


execute()
