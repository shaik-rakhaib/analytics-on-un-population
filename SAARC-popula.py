import csv
import matplotlib.pyplot as plt

saarc_countries = ['Afghanistan', 'India', 'Bangladesh',
'Bhutan', 'Maldives', 'Nepal', 'Pakistan',
'Sri Lanka']
total_pop_saarc = {}


def calc_saarc_popu():
    with open('population.csv', 'r', encoding='utf-8') as csv_file:
        file = csv.DictReader(csv_file)
        for row in file:
            if row['Region'] in saarc_countries:
                if row['Year'] not in total_pop_saarc:
                    total_pop_saarc[row['Year']] = int(float(row['Population']))
                else:
                    total_pop_saarc[row['Year']] += int(float(row['Population']))


def plot_popo_saarc():
    plt.figure(figsize=(12, 6))
    plt.title("Total Population of SAARC Countries across the years")
    plt.bar(list(total_pop_saarc.keys()), list(total_pop_saarc.values()))
    plt.xlabel("Years")
    plt.ylabel("Population")
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.show()


def execute():
    calc_saarc_popu()
    plot_popo_saarc()


execute()
