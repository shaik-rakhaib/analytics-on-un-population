import csv
import matplotlib.pyplot as plt

asean_countries = ['Thailand', 'Philippines', 'Malaysia', 'Singapore',
                 'Indonesia', 'Brunei Darussalam',
                 'Viet Nam', 'Lao People\'s Democratic Republic',
                'Cambodia', 'Myanmar']
asean_2014 = {}


def calc_asean_2014():
    with open('population.csv', 'r', encoding='utf-8') as csv_file:
        file = csv.DictReader(csv_file)
        for row in file:
            if row['Region'] in asean_countries:
                if row['Year'] == '2014':
                    if row['Region'] == 'Lao People\'s Democratic Republic':
                        asean_2014['Lao'] = int(float(row['Population']))
                    else:
                        asean_2014[row['Region']] = int(float(row['Population']))


def plot_asean_2014():
    plt.figure(figsize=(12, 6))
    plt.title("ASEAN Countries Population in 2014")
    plt.bar(list(asean_2014.keys()), list(asean_2014.values()))
    plt.xlabel('ASEAN Countries')
    plt.ylabel('Population in 2014')
    plt.tight_layout()
    plt.show()


def execute():
    calc_asean_2014()
    plot_asean_2014()


execute()
