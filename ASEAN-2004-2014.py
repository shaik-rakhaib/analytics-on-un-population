import csv
import matplotlib.pyplot as plt

asean_countries = ['Thailand', 'Philippines', 'Malaysia', 'Singapore',
                    'Indonesia', 'Brunei Darussalam', 'Viet Nam',
                    'Lao People\'s Democratic Republic',
                    'Cambodia', 'Myanmar']
asean_countries_years_population = {}


def calc_popu_asean():
    '''calculates population of ASEAN countries'''
    with open('population.csv', 'r', encoding='utf-8') as csv_file:
        population_file = csv.DictReader(csv_file)
        for row in population_file:
            year = int(row['Year'])
            coun = row['Region']
            popu = int(float(row['Population']))
            if year >= 2004 and year <= 2014 and coun in asean_countries:
                if year not in asean_countries_years_population:
                    asean_countries_years_population[year] = [coun, popu]
                else:
                    asean_countries_years_population[year].append([coun, popu])


year = list(asean_countries_years_population.keys())
print(asean_countries_years_population)
